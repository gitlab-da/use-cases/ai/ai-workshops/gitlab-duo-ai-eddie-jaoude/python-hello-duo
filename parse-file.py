# Parse the README.md 
# Print the headings 

import re

def get_headings(file_name):
    with open(file_name, 'r') as file:
        content = file.read()
    
    headings = re.findall(r'#+\s(.+)', content)
    return headings

def print_headings(headings):
    for heading in headings:
        print(heading)

headings = get_headings('README.md')
print_headings(headings)
