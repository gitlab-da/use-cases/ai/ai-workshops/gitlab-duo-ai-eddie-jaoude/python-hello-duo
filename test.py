import unittest
from regex import parse_ipv4

class TestParseIpv4(unittest.TestCase):

    def test_valid_ip(self):
        ip = "127.0.0.1"
        result = parse_ipv4(ip)
        self.assertEqual(result, [127, 0, 0, 1])

    def test_invalid_ip_too_many_octets(self):
        ip = "127.0.0.1.5"
        result = parse_ipv4(ip)
        self.assertIsNone(result)

    def test_invalid_ip_out_of_range_octet(self):
        ip = "127.0.0.256" 
        result = parse_ipv4(ip)
        self.assertIsNone(result)

    def test_invalid_ip_incomplete(self):
        ip = "127.0.0"
        result = parse_ipv4(ip)
        self.assertIsNone(result)

    def test_invalid_ip_out_of_range_octet2(self):
        ip = "256.1.1.1"
        result = parse_ipv4(ip)
        self.assertIsNone(result)

    def test_invalid_ip_too_few_octets(self):
        ip = "1.1.1" 
        result = parse_ipv4(ip)
        self.assertIsNone(result)

    def test_invalid_ip_too_many_octets2(self):
        ip = "1.1.1.1.1"
        result = parse_ipv4(ip) 
        self.assertIsNone(result)

if __name__ == '__main__':
    unittest.main()