# Generate a regex to parse IPv4 addresses
import re

def parse_ipv4(ip_str):
    pattern = r'^(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})$'
    match = re.match(pattern, ip_str)
    if match:
        octets = [int(octet) for octet in match.groups()]
        if all(0 <= octet <= 255 for octet in octets):
            return octets
    return None

if __name__ == '__main__':
    print(parse_ipv4('127.0.0.1'))
    print(parse_ipv4('127.0.0.1.5'))
    print(parse_ipv4('127.0.0.256'))
    print(parse_ipv4('127.0.0'))
    print(parse_ipv4('256.1.1.1'))
    print(parse_ipv4('1.1.1'))
    print(parse_ipv4('1.1.1.1.1'))

    