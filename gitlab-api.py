# Fetch all issue author names from a given issue ID from the GitLab API

import requests
import os 

project_id=57513099
issue_id=6
gl_token=os.environ.get("GITLAB_TOKEN")
gl_url=os.environ.get("GITLAB_URL")

def fetch_issue_authors(issue_id, gitlab_url, private_token):
    url = f"{gitlab_url}/api/v4/projects/{project_id}/issues/{issue_id}/notes?per_page=100"
    headers = {"Private-Token": private_token}
    response = requests.get(url, headers=headers)
    
    if response.status_code == 200:
        notes = response.json()
        authors = set()
        for note in notes:
            author = note["author"]["name"]
            authors.add(author)
        return list(authors)
    else:
        print(f"Error fetching issue authors: {response.status_code}")
        return []
    
 
list = fetch_issue_authors(issue_id, "https://gitlab.com", gl_token)

#print(list)

# format as list with new lines
# order by name 
sorted_list = sorted(list)

# print number of authors
print(len(sorted_list))

for author in sorted_list:
    print(author)




