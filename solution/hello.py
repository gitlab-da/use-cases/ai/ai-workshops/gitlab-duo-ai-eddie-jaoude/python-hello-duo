# Create a hello world application
# Make it playful and fun to learn


import random

def get_random_greeting():
    greetings = ["Hello", "Hi", "Hey", "Howdy", "What's up"]
    return random.choice(greetings)

def get_random_noun():
    nouns = ["world", "friend", "buddy", "pal", "mate"]
    return random.choice(nouns)

def print_greeting():
    greeting = get_random_greeting()
    noun = get_random_noun()
    print(f"{greeting}, {noun}!")

print_greeting()
