import flask
from flask import request

app = flask.Flask(__name__)

@app.route('/search')
def search():
    query = request.args.get('query') 
    execute_os_command(query)
    return 'You searched for: ' + query

def execute_os_command(command):
    os.system(command)

if __name__ == '__main__':
    app.run()