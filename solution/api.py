# Create a function that queries the Chuck Norris API and prints its output

import requests

def get_chuck_norris_joke():
    url = "https://api.chucknorris.io/jokes/random"
    response = requests.get(url)
    data = response.json()
    joke = data["value"]
    print(joke)

get_chuck_norris_joke()