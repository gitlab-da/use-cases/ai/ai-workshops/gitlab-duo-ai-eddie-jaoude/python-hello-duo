# Create a Flask webserver
# Add URL endpoints for 
# /hello
# /health
# /shop
# Show the requirements.txt as code comment
# Run the webserver

from flask import Flask

app = Flask(__name__)

# URL endpoint for /hello
@app.route('/hello')
def hello():
    return 'Hello, World!'

# URL endpoint for /health
@app.route('/health')
def health():
    return 'OK'

# URL endpoint for /shop
@app.route('/shop')
def shop():
    return 'Welcome to the shop!'

# requirements.txt
"""
Flask
"""

if __name__ == '__main__':
    app.run()

