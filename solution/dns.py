import io 

def resolve_dns_by_record(hostname, record_type):

    import dns.resolver

    try:
        resolver = dns.resolver.Resolver()
        if record_type == 'A':
            answer = resolver.resolve(hostname, 'A')
            return [ip.to_text() for ip in answer.rrset]
        elif record_type == 'AAAA':
            answer = resolver.resolve(hostname, 'AAAA')
            return [ip.to_text() for ip in answer.rrset]
        elif record_type == 'CNAME':
            answer = resolver.resolve(hostname, 'CNAME')
            return [cname.to_text() for cname in answer.rrset]
        else:
            return []
    except dns.resolver.NXDOMAIN:
        return []
    except dns.resolver.NoAnswer:
        return []
    except dns.exception.DNSException:
        return []

resolve_dns_by_record('gitlab.com', 'AAAA')