# Create a recursive Fibonacci calculation
def fibonacci(n):
    if n <= 1:
        return n
    else:
        return fibonacci(n-1) + fibonacci(n-2)

# Create a main function
def main():

    # Get user input for n
    n = int(input("Enter the number of terms: "))

    # Print the Fibonacci series up to n
    print("Fibonacci series:")
    for i in range(n):
        print(fibonacci(i), end=" ")

# Call the main function
if __name__ == "__main__":
    main()