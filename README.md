# Python Hello Duo

For instructions, follow https://gitlab.com/gitlab-da/use-cases/ai/ai-workshops/gitlab-duo-ai-eddie-jaoude/getting-started-exercises 

## Solution

```shell
cd solution/
```

On macOS:

```shell
brew install python

python3 -m pip install --user virtualenv

python3 -m venv myenv

source myenv/bin/activate

pip3 install -r requirements.txt
```

Run the code using

```shell
python3 <script>.py
```

## Test 