# Create a hello world function and say Hi Eddie

import os 

def hello_world(name):
    """Prints a greeting with the given name.
    
    Args:
        name (str): The name to greet.
        
    Returns:
        None
    """
    print(f"Hi {name}")

hello_world("Eddie")

# Create a function to greet two people

def greet_duo(name1, name2):
    print(f"Hi {name1} and {name2}")

greet_duo("Alice", "Bob")

